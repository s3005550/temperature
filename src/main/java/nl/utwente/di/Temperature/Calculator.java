package nl.utwente.di.Temperature;

public class Calculator {
    public double getFahrnheit(String input) {
        double celcius = Double.parseDouble(input);
        return celcius * (9.0/5.0) + 32.0;
    }
}
